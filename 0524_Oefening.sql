USE ModernWays;
SET SQL_SAFE_UPDATES = 0;

DELETE FROM Huisdieren
WHERE Baasje = "Thaïs" AND Soort = "Hond" 
OR Baasje = "Truus" AND Soort = "Kat";

SET SQL_SAFE_UPDATES = 1;