DROP USER IF EXISTS examen;

CREATE USER examen
IDENTIFIED BY 'corona';

GRANT EXECUTE ON PROCEDURE aptunes_examen.GetSongDuration
TO examen;