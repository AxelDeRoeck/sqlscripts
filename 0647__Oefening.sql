USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$

CREATE PROCEDURE `MockAlbumReleasesLoop` (
IN extraReleases INT
)
BEGIN
	DECLARE counter INT DEFAULT 0;
    
	loop_releases:LOOP
		CALL MockAlbumReleaseWithSucces(@succes);
        IF @succes = 1 THEN
			SET counter = counter + 1;
        END IF;
        IF counter >= extraReleases THEN
			LEAVE loop_releases;
        END IF;
    END LOOP;
END$$

DELIMITER ;

