USE ModernWays;

CREATE TABLE Uitleningen(
Leden_Id INT NOT NULL,
CONSTRAINT fk_Uitleningen_Leden FOREIGN KEY (Leden_Id) REFERENCES Leden(Id),
Boeken_Id INT NOT NULL,
CONSTRAINT fk_Uitleningen_Boeken FOREIGN KEY (Boeken_Id) REFERENCES Boeken(Id),
StartDatum DATE NOT NULL,
EindDatum DATE NULL
)