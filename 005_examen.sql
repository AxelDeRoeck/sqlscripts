USE `aptunes_examen`;
DROP procedure IF EXISTS `GetSongDuration`;

DELIMITER $$
USE `aptunes_examen`$$
CREATE PROCEDURE `GetSongDuration` (
IN bandId INT
)

-- ik denk dat de parameter de bandId is ipv albumId omdat de opgave mij vreemd lijkt
-- dat het opeens een opgegeven band is en geen album meer

BEGIN
	DECLARE totaalDuurTijd INT DEFAULT 0;
    DECLARE klaar INT DEFAULT 0;
    DECLARE liedLengte INT DEFAULT 0;
    DECLARE liedBandId INT DEFAULT 0;
    
    DECLARE huidigLied CURSOR FOR 
    SELECT Lengte, Bands_Id FROM Liedjes;
    
    DECLARE CONTINUE HANDLER FOR
    NOT FOUND SET klaar = 1;
    
    OPEN huidigLied;
    
    telLengte: LOOP
		FETCH huidigLied INTO liedLengte, liedBandId;
        IF (klaar = 1) THEN
			LEAVE telLengte;
        END IF;
        IF (liedBandId = bandId) THEN
			SET totaalDuurTijd = totaalDuurTijd + liedLengte;
        END IF;
        
    END LOOP telLengte;
    
    CLOSE huidigLied;
    
	IF (totaalDuurTijd > 60) THEN
		SELECT "Lange duurtijd" AS "Duurtijd";
	ELSE 
		SELECT "Normale duurtijd" AS "Duurtijd";
	END IF;
    
END$$

DELIMITER ;

