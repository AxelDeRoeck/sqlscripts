USE ModernWays;
CREATE VIEW AuteursBoeken
AS
SELECT Boeken.Id, Boeken.Titel AS "Titel", CONCAT(Personen.Voornaam, " " , Personen.Familienaam) AS "Auteur"
FROM Publicaties
INNER JOIN Personen
ON Personen_Id = Personen.Id
INNER JOIN Boeken
ON Boeken_Id = Boeken.Id;
