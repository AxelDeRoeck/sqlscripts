USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumreleases` ()
BEGIN
	DECLARE randomGetal INT DEFAULT 0;
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    DECLARE counter INT DEFAULT 0;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN
		ROLLBACK;
        SELECT 'Nieuwe releases konden niet worden toegevoegd' Message;
    END;
    
    START TRANSACTION;
    
	REPEAT 
    
		SELECT COUNT(*) FROM Albums INTO numberOfAlbums;
		SELECT COUNT(*) FROM Bands INTO numberOfBands;
		SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
		SET randomBandId = FLOOR(RAND() * numberOfBands) + 1; 
		
		IF (counter >= 2) THEN
			SET randomGetal = FLOOR(RAND()*3+1);
			IF (randomGetal = 1) THEN
				SIGNAL SQLSTATE '45000';
			END IF;
		END IF;
		
		IF (
		(SELECT Albums_Id FROM Albumreleases WHERE Albums_Id = randomAlbumId) IS NULL 
		AND 
		(SELECT Bands_Id FROM Albumreleases WHERE Bands_Id = randomBandId) IS NULL
		) 
		THEN
			INSERT INTO AlbumReleases(Albums_Id, Bands_Id)
			VALUES (randomAlbumId, randomBandId);
		END IF;
    
		SET counter = counter + 1;
		UNTIL counter >= 3
    END REPEAT;
    
    COMMIT;
END$$

DELIMITER ;

