USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumRelease`()
BEGIN
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    
	SELECT COUNT(*) FROM Albums INTO numberOfAlbums;
    SELECT COUNT(*) FROM Bands INTO numberOfBands;
    
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
    SET randomBandId = FLOOR(RAND() * numberOfBands) + 1; 
    
    IF (
    (SELECT Albums_Id FROM Albumreleases WHERE Albums_Id = randomAlbumId) IS NULL 
    AND 
	(SELECT Bands_Id FROM Albumreleases WHERE Bands_Id = randomBandId) IS NULL
    ) 
    THEN
		INSERT INTO AlbumReleases(Albums_Id, Bands_Id)
        VALUES (randomAlbumId, randomBandId);
	END IF;
END$$

DELIMITER ;

