CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(IN titel VARCHAR(50), IN bands_Id INT)
BEGIN
  START TRANSACTION;
  INSERT INTO Albums (
  Titel
  ) VALUES (
  titel
  );
  INSERT INTO Albumreleases (
  Bands_Id, 
  Albums_Id
  ) VALUES (
  bands_Id,
  LAST_INSERT_ID()
  );
  COMMIT;
END