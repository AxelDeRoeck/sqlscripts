USE ModernWays;
CREATE TABLE Lector (
Personeelsnummer INT AUTO_INCREMENT PRIMARY KEY,
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);
CREATE TABLE Student (
Studentennummer INT AUTO_INCREMENT PRIMARY KEY,
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);
CREATE TABLE Opleiding (
Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL PRIMARY KEY
);
CREATE TABLE Vak (
Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL PRIMARY KEY
);
ALTER TABLE Student 
ADD COLUMN Opleiding_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
ADD COLUMN Semester TINYINT UNSIGNED NOT NULL,
ADD CONSTRAINT fk_Student_Opleiding FOREIGN KEY (Opleiding_Naam) REFERENCES Opleidingen(Naam);

CREATE TABLE LectorGeeftVak (
Lector_Personeelsnummer INT NOT NULL,
Vak_Naam VARCHAR(100) CHAR SET utf8mb4,
CONSTRAINT fk_LectorGeeftVak_Lector
FOREIGN KEY (Lector_Personeelsnummer)
REFERENCES Lector(Personeelsnummer),
CONSTRAINT fk_LectorGeeftVak_Vak
FOREIGN KEY (Vak_Naam)
REFERENCES Vak(Naam)
);

CREATE TABLE VakOpleidingsOnderdeelOpleiding (

);
