USE ModernWays;
CREATE VIEW AuteursBoekenRatings
AS
SELECT AuteursBoeken.Auteur, Auteursboeken.Titel, GemiddeldeRatings.Rating 
FROM AuteursBoeken
INNER JOIN GemiddeldeRatings ON GemiddeldeRatings.Boeken_Id = AuteursBoeken.Boeken_Id