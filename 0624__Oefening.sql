USE aptunes;

CREATE INDEX TitelNaamLengteIdx
ON Liedjes(Titel,Naam,Lengte);
CREATE INDEX NaamIdx
ON Bands(Naam);
-- dit maakt het sneller met een verschil van 0.004 ik heb geen idee wat ik juist moet doen --

SELECT Titel, Naam, Lengte FROM Liedjes
inner join Bands
on Liedjes.Bands_Id = Bands.Id
where Titel like 'A%'
order by Lengte;