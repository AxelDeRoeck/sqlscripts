USE aptunes;

CREATE INDEX VoornaamFamilienaamIdx
ON Muzikanten(Voornaam,Familienaam);

select Voornaam, Familienaam, count(Lidmaatschappen.Muzikanten_Id)
from Muzikanten inner join Lidmaatschappen
on Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
group by Familienaam, Voornaam
order by Voornaam, Familienaam;