USE ModernWays;
SELECT Huisdieren.Naam AS "Naam Huisdier", Baasjes.baasje AS "Naam Baasje"
FROM Huisdieren
CROSS JOIN Baasjes
WHERE Huisdieren.Id = Baasjes.Huisdieren_Id;