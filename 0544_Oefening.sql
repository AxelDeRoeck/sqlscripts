USE ModernWays;

SELECT Artiest, AVG(Aantalbeluisteringen)
FROM Liedjes
GROUP BY Artiest
HAVING length(Artiest) >= 10;