CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(
IN someDate DATE,
OUT numberCleaned INT
)
BEGIN
DECLARE numberFull INT DEFAULT COUNT(Lidmaatschappen);

SET SQL_SAFE_UPDATES = 0;
DELETE FROM Lidmaatschappen
WHERE Einddatum < someDate;
SELECT ROW_COUNT() 
INTO numberCleaned;
SET SQL_SAFE_UPDATES = 1;

SELECT numberFull - COUNT(Lidmaatschappen)
INTO numberCleaned;
END 