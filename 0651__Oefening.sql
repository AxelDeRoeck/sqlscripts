USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration` (
IN albumId INT,
OUT totalDuration SMALLINT UNSIGNED)
BEGIN
	DECLARE liedLengte TINYINT UNSIGNED;
    DECLARE huidigLiedAlbumId INT DEFAULT 0;
    DECLARE klaar INT DEFAULT 0;
    
    DECLARE huidigLied
    CURSOR FOR SELECT Lengte, Albums_Id FROM Liedjes;
    
    DECLARE CONTINUE HANDLER
    FOR NOT FOUND SET klaar = 1;
    
    OPEN huidigLied;
    
    SET totalDuration = 0;
    getLengte: LOOP
		FETCH huidigLied INTO liedLengte,huidigLiedAlbumId;
        
		IF (huidigLiedAlbumId = albumId) THEN
			SET totalDuration = totalDuration + liedLengte;
		END IF;
        
		IF (klaar = 1) THEN
			LEAVE getLengte;
        END IF;
        
    END LOOP getLengte;

	CLOSE huidigLied;
END$$

DELIMITER ;