USE ModernWays;
SELECT DISTINCT Platformen.Naam 
FROM Releases
LEFT JOIN Platformen ON Platformen.Id = Platformen_Id
RIGHT JOIN Games ON Games.Id = Games_Id
WHERE Games_Id IS NOT NULL
