USE aptunes;

CREATE INDEX NaamIdx
ON Genres(Naam);
CREATE INDEX TitelIdx
ON Liedjes(Titel);

select Titel, Naam
from Liedjesgenres inner join Liedjes
on Liedjesgenres.Liedjes_Id = Liedjes.Id
inner join Genres
on Liedjesgenres.Genres_Id = Genres.Id
where Naam = 'Rock';