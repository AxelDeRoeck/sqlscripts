USE ModernWays;
SELECT DISTINCT Voornaam
FROM Studenten
WHERE Voornaam IN (SELECT Voornaam FROM Personeelsleden) 
AND Voornaam IN (SELECT Voornaam FROM Directieleden);