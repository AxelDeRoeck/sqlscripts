USE `aptunes_examen`;
DROP procedure IF EXISTS `PopulateLiedjesGenres`;

DELIMITER $$
USE `aptunes_examen`$$
CREATE PROCEDURE `PopulateLiedjesGenres` ()
BEGIN
	DECLARE numberOfLiedjes INT DEFAULT 0;
    DECLARE numberOfGenres INT DEFAULT 0;
    DECLARE randomLiedjeId INT DEFAULT 0;
    DECLARE randomGenreId INT DEFAULT 0;
    
    SELECT COUNT(*) INTO numberOfLiedjes FROM Liedjes;
    SELECT COUNT(*) INTO numberOfGenres FROM Genres;
    
    SET randomLiedjeId = FLOOR(RAND() * numberOfLiedjes) + 1;
    SET randomGenreId = FLOOR(RAND() * numberOfGenres) + 1;
    
    IF (randomLiedjeId, randomGenreId) NOT IN (SELECT * FROM Liedjesgenres)
    THEN
		INSERT INTO Liedjesgenres(Liedjes_Id, Genres_Id)
        VALUES (randomLiedjeId, randomGenreId);
	END IF;
END$$

DELIMITER ;

