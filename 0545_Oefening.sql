USE ModernWays;

SELECT Artiest, SUM(Aantalbeluisteringen)
FROM Liedjes
WHERE length(Artiest) >= 10
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) >= 100;